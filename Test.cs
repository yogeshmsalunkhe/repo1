using System;
using System.Threading.Tasks;

namespace TestProject1
{
    public class Class1
    {
        public string Name { get; set; }

        public async void Method1()
        {
            //var d = new FileStyleUriParser();
            int target = -5;
            int num = 3;

            target = -num;
            target = +num;
        }

        public void Method2()
        {
            for (int i = 1; i <= 5; i++)
            {
                Console.WriteLine(i);
                if (1 < 3)
                {
                    i = 20;
                }
            }
        }

        public bool Login()
        {
            string userName = "admin";
            string password = "admin123";
        }

        public Task<object> Method3Async(int i)
        {
            //if (i == 5)
            //{
            //    return "fgf";
            //}
            return null;
        }
    }
}
